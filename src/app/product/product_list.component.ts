import {Component} from '@angular/core'
import { Iproduct } from './product';
@Component({
    selector: 'pm-products',
    templateUrl:'./product_list.component.html'
})

export class ProductListComponent{
    title: String = "Product List" ;
    showImage: boolean = false;
    listFilter: String ="";
    products: Iproduct[]=[
        {
            "productId":2,
            "productName":"hammmer",
            "productCode":"Ng-898",
            "releaseDate":"march 7, 2018",
            "description":"very good hammer",
            "price":99.99,
            "starRating":4.8,
            "imageUrl":"C:\Users\Saurav\Downloads\hammer.png"
        },

        {
            "productId":3,
            "productName":"ball",
            "productCode":"Hg-990",
            "releaseDate":"march 17, 2018",
            "description":"very good ball",
            "price":29.99,
            "starRating":4.4,
            "imageUrl":"C:\Users\Saurav\Desktop\texas.png   "
        }
    ];  
        toggleImage(): void{
            this.showImage=!this.showImage;
        }
}